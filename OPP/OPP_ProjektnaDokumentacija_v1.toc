\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{5}{chapter.2}%
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{11}{subsection.3.1.1}%
\contentsline {subsubsection}{Opis obrazaca uporabe}{11}{subsubsection*.2}%
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{20}{subsubsection*.3}%
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{23}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{27}{section.3.2}%
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{28}{chapter.4}%
\contentsline {section}{\numberline {4.1}Baza podataka}{30}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{31}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{36}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Dijagram razreda}{37}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Dijagram razreda Firebase usluga}{37}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Dijagram razreda paketa \textit {authentication}}{40}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Dijagram razreda paketa \textit {organizator}}{42}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Dijagram razreda paketa \textit {voditelj}}{43}{subsection.4.2.4}%
\contentsline {subsection}{\numberline {4.2.5}Dijagram razreda paketa \textit {izvođač}}{45}{subsection.4.2.5}%
\contentsline {subsection}{\numberline {4.2.6}Dijagram razreda paketa \textit {common}}{47}{subsection.4.2.6}%
\contentsline {section}{\numberline {4.3}Dijagram stanja}{50}{section.4.3}%
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{52}{section.4.4}%
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{53}{section.4.5}%
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{55}{chapter.5}%
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{55}{section.5.1}%
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{56}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{56}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Ispitivanje sustava}{62}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{67}{section.5.3}%
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{68}{section.5.4}%
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{70}{chapter.6}%
\contentsline {chapter}{Popis literature}{72}{chapter*.4}%
\contentsline {chapter}{Indeks slika i dijagrama}{74}{chapter*.5}%
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{75}{chapter*.6}%
